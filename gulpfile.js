const gulp = require('gulp');
const sass = require('gulp-sass');
const minifyCSS = require('gulp-csso');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const nunjucks = require('gulp-nunjucks-render');
const markdown = require('gulp-markdown');
const rename = require('gulp-rename');
const header = require('gulp-header');
const footer = require('gulp-footer');
const frontMatter = require('gulp-front-matter');
const imagemin = require('gulp-imagemin');

gulp.task('css', function() {
  return gulp.src('assets/main.scss')
    .pipe(sass())
    .pipe(minifyCSS())
    .pipe(gulp.dest('public/css'))
});

gulp.task('img', function() {
  return gulp.src('assets/images/*')
    .pipe(imagemin())
    .pipe(gulp.dest('public'))
})

gulp.task('marked', function() {
  return gulp.src('assets/markdown/**/*.md')
    .pipe(frontMatter({
      property: 'frontMatter',
      remove: true
    }))
    .pipe(markdown())
    .pipe(rename(function (path) {
      path.basename = "index";
      path.extname = ".html";
    }))
    .pipe(header('{% extends \"default.njk\" %}\r\n{% block content %}\r\n<article>'))
    .pipe(footer('</article>\r\n{% endblock %}'))
    .pipe(gulp.dest('assets/pages'))
})

gulp.task('js', function(){
  return gulp.src('client/javascript/*.js')
    .pipe(sourcemaps.init())
    .pipe(concat('app.min.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/js'))
});

gulp.task('html', ['marked'], function() {
  return gulp.src([
    'assets/pages/*.+(html|njk)',
    'assets/pages/**/*.+(html|njk)',
  ])
    .pipe(nunjucks({
      path: ['assets/templates']
    }))
    .pipe(rename(function(path) {
      path.extname = ".html"
    }))
    .pipe(gulp.dest('public'))
});

gulp.task('watch', function() {
  gulp.watch(['assets/*', 'assets/pages/*', 'assets/templates/*'], ['css', 'js', 'html', 'img']);
});

gulp.task('default', ['css', 'js', 'html', 'img']);