<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/main.css" type="text/css">
    <title>mirango.io | homepage</title>
  </head>

<header>
    <h1 id="title"><a href="/">mirango.io</a></h1>
    <nav>
  <a href="/about">~/about</a>
  <a href="https://www.bitbucket.org/sdwalsh">~/bitbucket</a>
  <a href="https://www.keybase.io/sdwalsh">~/keybase</a>
  <a href="https://www.twitter.com/mirangomadness">~/twitter</a>
</nav>
  </header>


<article><h1 id="properly-configuring-nginx-for-https">Properly Configuring Nginx for HTTPS</h1>
<p>While setting up <a href="http://www.mirango.io">www.mirango.io</a>, I chose to adopt HTTPS in order to ensure privacy of readers.  Here&#39;s a short guide I put together while setting up my server.</p>
<h2 id="http-to-https-redirects">HTTP to HTTPS redirects</h2>
<p>In order to enforce HTTPS, it&#39;s necessary to redirect all HTTP traffic (port 80) to HTTPS (port 443).  On Nginx servers, this is accomplished within the server block:</p>
<pre><code class="lang-Nginx">server {
       listen         80;
       server_name    www.mirango.com;
       # 301 Permanent Redirect
       return         301 https://www.mirango.io$request_uri;
    }
</code></pre>
<p>When users visit <code>http://www.mirango.com:80</code> they receive a <code>301 Permanent Redirect</code> to <code>https://www.mirango.io:443</code>.  301 redirects are recommended by both Nginx and <a href="https://support.google.com/webmasters/answer/93633?hl=en">Google</a> to redirect traffic from any non-preferred domain.  Similar code is used to specify the canonical domain for mirango.io (www over non-www).</p>
<h2 id="ocsp-stapling">OCSP Stapling</h2>
<p><a href="http://tools.ietf.org/html/rfc2560">OCSP</a>, Online Certificate Status Protocol, is a protocol used to check revocation status of certificates - specifically designed as an alternative to certificate revocation lists.  OCSP responders are generally run by certificate authorities, but are often slow and prone to failures.</p>
<p>Enter <a href="http://tools.ietf.org/html/rfc6066">OCSP stapling</a>.  Also known as TLS Certificate Status Request extension,  OCSP stapling allows web servers to cache the OCSP record clients normally receive from an OCSP responder, saving network resources.  The cached record is sent during the TLS handshake, but only if the client requests it during the extended ClientHello (status_request).</p>
<pre><code class="lang-Nginx">ssl_stapling on;
ssl_stapling_verify on;
# make sure full_chain.pem contains all certificates
ssl_trusted_certificate /etc/nginx/ssl/full_chain.pem;
</code></pre>
<h2 id="http-strict-transport-security">HTTP Strict Transport Security</h2>
<p>With the threat of <a href="https://www.blackhat.com/presentations/bh-dc-09/Marlinspike/BlackHat-DC-09-Marlinspike-Defeating-SSL.pdf">downgrade attacks</a> on TLS (transparent conversion of HTTPS to HTTP in a MitM style attack), it is recommended to implement HTTP Strict Transport Securit (HSTS). HSTS enables a website to declare it is only accessible through secure connections.</p>
<p>To enable HSTS, simply add the following line to the server block:</p>
<pre><code class="lang-Nginx"># enable HSTS for one year on *.mirango.io
add_header Strict-Transport-Security &quot;max-age=31536000; includeSubDomains&quot;;
</code></pre>
<h2 id="ssl-protocols-and-poodle">SSL Protocols and POODLE</h2>
<p>The threat from <a href="https://www.openssl.org/~bodo/ssl-poodle.pdf">POODLE</a>, Padding Oracle On Downgraded Legacy Encryption, has necessitated the retirement of SSLv3 support (TLSv1 actually replaced SSLv3 15 years ago!).  The vulnerability exploited by POODLE allows a MitM attacker to decrypt secure HTTP cookies using a padding oracle attack against CBC-mode (cipher block chaining) ciphers.</p>
<p>POODLE works since SSLv3 decrypts before verifying the MAC (used to detect tampering) and ignores the contents of padding bytes, only looking for the last byte containing the length of the padding.  For a more detailed explanation of the vulnerability that allows POODLE to work, <a href="http://www.educatedguesswork.org/2011/09/security_impact_of_the_rizzodu.html">click here</a>.</p>
<p>Securing a web server against this style attack is as simple as removing SSLv3 support.</p>
<pre><code class="lang-Nginx"># use TLSv1+
ssl_protocols           TLSv1 TLSv1.1 TLSv1.2;
</code></pre>
<h2 id="public-key-pinning-extension-for-http-hpkp-">Public Key Pinning Extension for HTTP (HPKP)</h2>
<p>HPKP, Public Key Pinning Extension, is a HTTP header that instructs clients to remember the server&#39;s cryptographic identity.  When a client visits the server again, it expects a certificate containing the public key stored from the original visit.  HPKP reduces the ability to <a href="http://arstechnica.com/business/2012/02/critics-slam-ssl-authority-for-minting-cert-used-to-impersonate-sites/">issue forged certificates</a> for a website.  It should be noted that there is an inherent weakness to key pinning. Since key pinning is a trust on first use security mechanism a client cannot detect a MitM attack (using a forged certificate) when visiting a site for the first time.</p>
<p>HPKP requires the base64-encode of SHA256 hash of the public key, certificate signing request, or the certificate.  <a href="https://developer.mozilla.org/en-US/docs/Web/Security/Public_Key_Pinning">Mozilla provides a short guide</a>:</p>
<pre><code class="lang-bash">#Given the public key my-key-file.key:
openssl rsa -in my-key-file.key -outform der -pubout | openssl dgst -sha256 -binary | openssl enc -base64

#Given the CSR my-signing-request.csr:
openssl req -in my-signing-request.csr -pubkey -noout | openssl rsa -pubin -outform der | openssl dgst -sha256 -binary | openssl enc -base64

#Or given the certificate my-certificate.crt
openssl x509 -in my-certificate.crt -pubkey -noout | openssl rsa -pubin -outform der | openssl dgst -sha256 -binary | openssl enc -base64
</code></pre>
<pre><code class="lang-Nginx">add_header Public-Key-Pins &#39;pin-sha256=&quot;BASE64-KEY-INFORMATION&quot;; pin-sha256=&quot;BASE64-KEY-INFORMATION&quot;; max-age=5184000; includeSubDomains&#39;;
</code></pre>
<h2 id="logjam-vulnerability-freak">Logjam Vulnerability / FREAK</h2>
<p>A recent <a href="https://weakdh.org/imperfect-forward-secrecy.pdf">paper</a> published by INRIA, Microsoft Research, University of Pennsylvania, Johns Hopkins, and University of Michigan exposes a flaw in TLS.  Logjam is similar to FREAK but, rther than an implementation vulnerability like FREAK, logjam exploits a vulnerability in the TLS protocol itself.</p>
<p>The vulnerability relies on old export ciphers that were developed during the 1990&#39;s when the United States banned the export of &quot;strong ciphers&quot; (<a href="https://en.wikipedia.org/wiki/Arms_Export_Control_Act">Arms Export Control Act</a>).  These export ciphers were specifically designed to be weak and today can be broken with home computers.</p>
<p>This leads into the vulnerability.</p>
<p>During a man in the middle attack, an attacker can intercept a client connection and replace accepted ciphers with DHE_EXPORT.  The server will then pick 512-bit parameters, finish remaining computations, and sign the parameters.</p>
<p>The original client, unaware of the MitM attack, believes the server picked a DHE (not DHE_EXPORT) key exchange.  At this point, since the connection uses insecure ciphers, the attacker can easily recover the connection key.  Over 8% of top one million HTTPS websites are vulnerable.</p>
<p>It&#39;s now believed that in addition to export Diffie-Hellman, non-export Diffie-Hellman is possibly vulnerable to <a href="http://www.spiegel.de/international/germany/inside-the-nsa-s-war-on-internet-security-a-1010361.html">nation-state surveillance</a>.  Researchers speculate that the NSA has completed the pre computation (estimated at 48 million core years) of a few common 1024-bit parameters (non unique prime numbers that are shared by many servers) and can now easily break new discrete logarithms.  If the cryptography algorithm is broken, that means that forward security is no longer maintained.</p>
<p><a href="https://wiki.mozilla.org/Security/Server_Side_TLS#Recommended_configurations">Mozilla</a> and <a href="https://weakdh.org/sysadmin.html">others</a> recommend disabling any export ciphers and using a large prime, preferably unique, for Diffie-Hellman key exchanges.</p>
<pre><code class="lang-bash"># generate unique prime for Diffie Hellman Exchanges
openssl dhparam -out dhparam.pem 4096
</code></pre>
<pre><code class="lang-Nginx"># prefer Eliptic Curve Diffie Hellman Exchanges
ssl_ciphers &#39;ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA&#39;;

ssl_prefer_server_ciphers on;

# uniquely generated prime
ssl_dhparam DHPARAMS LOCATION;
</code></pre>
<h2 id="finished-nginx-server-block-configuration">Finished Nginx Server Block Configuration</h2>
<pre><code class="lang-Nginx">server {
    # listen on port 443 using ssl and spdy
    listen                  443 ssl spdy default_server;
    ssl_certificate         CERTIFICATE LOCATION;
    ssl_certificate_key     KEY LOCATION;
    ssl_session_cache       shared:SSL:20m;
    ssl_session_timeout     10m;
    ssl_protocols           TLSv1 TLSv1.1 TLSv1.2;
    # recommended secure ciphers
    ssl_prefer_server_ciphers on;
    ssl_ciphers &#39;ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA&#39;;
    # large prime for Diffie-Hellman
    ssl_dhparam DHPARAMS LOCATION;
    keepalive_timeout       70;
    # HTTP Strict Transport Security
    add_header Strict-Transport-Security &quot;max-age=31536000; includeSubDomains&quot;;
    # key pinning
    add_header Public-Key-Pins &#39;pin-sha256=&quot;BASE64-KEY-INFORMATION&quot;; pin-sha256=&quot;BASE64-KEY-INFORMATION&quot;; max-age=5184000; includeSubDomains&#39;;

    location / {
        root /home/www/public_html/;
        index index.html;
        }
    }
</code></pre>
</article>


<footer>
  <div id="name">
    Sean Walsh —
  </div>
  <div id="email">
    sdwalsh@mirango.io
  </div>
  <div id="city">
    Boston, MA
  </div>
  <div class="commit-data">
    <a href="https://bitbucket.org/sdwalsh/mirango"><span id="commit-hash"></span></a>
    <div>
      <span id="commit-date"></span>
    </div>
  </div>
  <script type="text/javascript" src="/all.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/vs.min.css" type="text/css">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>
</footer>
</html>